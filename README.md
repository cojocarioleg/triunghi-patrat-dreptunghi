import com.sun.deploy.net.proxy.WDefaultBrowserProxyConfig;
import java.util.Scanner;
import static java.lang.Math.sqrt;
public class Figuri {
    public static void main(String[] args) {
        boolean val = true;
        while (val) {
            Message.SayHello();
            Scanner s = new Scanner(System.in);
            BeginVariabile VarianVariable = new BeginVariabile();
            VarianVariable.setVar(s.nextLine());
            switch (VarianVariable.getVar()) {
                case "T"://triunghi
                    Triunghi triunghi = new Triunghi();
                    Message.InsertFigure("triangle");
                    Message.InsertLater(1);
                    triunghi.setLatura1(s.nextDouble());
                    if (triunghi.getLatura1() > 0) {
                        Message.InsertLater(2);
                        triunghi.setLatura2(s.nextDouble());
                        if (triunghi.getLatura2() > 0) {
                            Message.InsertLater(3);
                            triunghi.setLatura3(s.nextDouble());
                            if (triunghi.getLatura3() > 0) {
                                if (Triunghi.condiiiExistenta(triunghi.getLatura1(), triunghi.getLatura2(), triunghi.getLatura3())) {
                                    Triunghi.Calcul(triunghi.getLatura1(), triunghi.getLatura2(), triunghi.getLatura3());
                                } else
                                    System.out.println("imi pare rau marimea laturilor introduse nu corespund conditiilor");
                            }//end tri if
                            else System.out.println("Insert negativ later");
                        }//end seconds if
                        else System.out.println("Insert negativ later");
                    } //end first if
                    else System.out.println("Insert negativ later");
                    break;
                case "S": //patrat
                    Square square = new Square();
                    Message.InsertFigure("square");
                    Message.InsertLater(1);
                    square.setLatura(s.nextDouble());
                    if (square.getLatura() > 0) {
                        Square.Calcul(square.getLatura());
                    } else System.out.println("you insert negativ later");
                    break;
                case "R":
                    Right_Angle right_angle = new Right_Angle();
                    Message.InsertFigure("Right_Angle");
                    Message.InsertLater(1);
                    right_angle.setLater1(s.nextDouble());
                    if (right_angle.getLater1() > 0) {
                        Message.InsertLater(2);
                        right_angle.setLater2(s.nextDouble());
                        if (s.nextDouble() > 0) {
                            if (Right_Angle.Valid(right_angle.getLater1(), right_angle.getLater2())) {
                                Right_Angle.calcul(right_angle.getLater1(), right_angle.getLater2());
                            } else System.out.println("ati introdus date gresite");
                        } //end seconds if
                    }//end first if
                    else System.out.println("insert negativ later");
                    break;
                default:
                    System.out.println("you dont selecet corect option, please select corect option");
            }
            System.out.println(" if you whant to continuie ___ Y __ or not pres ___ n __");
            switch (s.next()) {
                case "Y":
                    System.out.println("you whant to continue, please isert corect options");
                    val = true;
                    break;
                case "n":
                    System.out.println("you dont whant to continue, By");
                    val = false;
                    break;
            }
        }
    }
}
class Right_Angle{//calcul dreptunghi
    private  double later1, later2;
    public void setLater1(double later1) {
        this.later1 = later1;
    }
    public double getLater1() {
        return later1;
    }
    public void setLater2(double later2) {
        this.later2 = later2;
    }
    public double getLater2() {
        return later2;
    }
    static boolean Valid(double a, double b){
        boolean f;
        if ((a!=b)&(a>0)&(b>0)){
            f=true;
        } else f = false;
        return f;
    }
    static void calcul(double a, double b){
        double p = 2* (a+b);
        double Aria = a*b;
        System.out.println("perimetru dreptunghiului este egal = "+p);
        System.out.println("Aria dreptunghiului este egal = " + Aria);
    }
}
class Triunghi{//calculare triunghi
    private double latura1, latura2, latura3;
    public void setLatura1(double latura1) {
        this.latura1 = latura1;
    }
    public double getLatura1() {
        return latura1;
    }
    public void setLatura3(double latura3) {
        this.latura3 = latura3;
    }
    public double getLatura3() {
        return latura3;
    }
    public void setLatura2(double latura2) {
        this.latura2 = latura2;
    }
    public double getLatura2() {
        return latura2;
    }
    static void Calcul( double a, double b, double c){
        double p = (a + b + c) / 2;
        double Aria = sqrt(p*(p-a)*(p-b)*(p-c));
        System.out.println("Aria triunghilui este egala cu = "+ Aria);
        System.out.println("Perimetrul triunghilui este egal cu =" + p);
    }
    static boolean condiiiExistenta(double a, double b, double c){
        //daca a <b+c şi b<a+c şi c<a+b
        boolean verfication;
        if ((a <b+c)&(b<a+c)&(c<a+b)){
            verfication = true;
        } else verfication = false;
        return verfication;
    }
}
class Square{//calculare patratul
    private double latura;
    public void setLatura(double latura) {
        this.latura = latura;
    }
    public double getLatura() {
        return latura;
    }
    static void Calcul (double latura){
        double p = 4 * latura;//perimetru
        double a = latura * latura; // aria
        double d =  (latura * sqrt(2));// diagonala
        System.out.println("perimetrul patratului este egal = " + p);
        System.out.println("aria patratului este egal = " + a);
        System.out.println("diagonala patratului este = " +d);
    }
}
class BeginVariabile{
    String Var;
    public void setVar(String var) {  Var = var; }
    public String getVar() { return Var;  }
}
class Pozitiv{
    static boolean pozitiv(double a){
        boolean c;
        if (a>0) c = true;
        else  c = false;
        return c;
    }
}
class Message {
    static void SayHello() {
        System.out.println("This program rezolv all about your geometric figure");
        System.out.println("Select your geometric figure");
        System.out.println("if you whant select triangle insert liter ===== T");
        System.out.println("if you whant select square insert liter ===== S");
        System.out.println("if you whant select Right_Angle insert liter ===== R");
    }
    static void InsertFigure(String f){
        System.out.println("you selected " + f + " please insert laters of the " +f);
    }
    static void InsertLater(int a){
        System.out.println("insert later nr.= " + a );
    }
}